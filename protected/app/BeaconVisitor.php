<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BeaconVisitor extends Model
{
    public $timestamps = false;
}
